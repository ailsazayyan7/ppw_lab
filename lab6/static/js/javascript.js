$(function() {
  $(".view").on( "click", function() {
    $(this).next().slideToggle(250);
    $fexpand = $(this).find(">:first-child");
    if ($(this).hasClass('opened')) {
        $(this).removeClass('opened');
    } else {
        $(this).addClass('opened');
    };
  });

    var switchContainer = $('.switch-container');

    switchContainer.on('click', function() {
    var body      = $('body'),
        onSwitch  = $('.switch'),
        container = $('.data');
      
      $(this).toggleClass('on-indicator');
      onSwitch.toggleClass('switched-on');
      body.toggleClass('night-mode');
      container.toggleClass('night-mode-text');
    });
});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 1000);
}

function showPage() {
    document.getElementById("loading").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}



 $(function(){
    $.ajax({
      type : "GET",
      url: '/data',
      dataType: 'json',
      success: function(data){
        var result = ''
        var temp = data['items']
        for (var i = 0; i<temp.length; i++) {
          item = temp[i]
          var img = item['volumeInfo']['imageLinks']['smallThumbnail']
          var title = item['volumeInfo']['title']
          var author = item['volumeInfo']['authors']
          var desc = item['volumeInfo']['description']

          result += "<tr><td><img src =" + img + "></td>"
          result += "<td>" + title + "</td>"
          result += "<td>" + author + "</td>"
          result += "<td>" + desc + "</td>"
          result += "<td><button type='button' class='tombol btn btn-dark'>Favorite</button></td></tr>"
        }
        $("#bt").html(result)
      }
    });
  });
  var fav = 0;
  $(document).on('click', '.tombol', function(){
    if ($(this).hasClass('clicked')) {
      fav--;
      $(this).removeClass('clicked');
      $(this).css("background-color","#343a40");
      $(this).css("color", "white");

    }
    else{
      fav++;
      $(this).addClass('clicked');
      $(this).css("background-color", "yellow");
      $(this).css("color", "black");
    }
    $("#jml").html(fav)
  });

