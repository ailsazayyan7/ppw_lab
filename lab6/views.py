from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .forms import The_Status
from .models import Status
import requests
# Create your views here!
response = {'author' : 'ailsa'}

def index(request):
	response['stat_form'] = The_Status
	response['data_form'] = Status.objects.all()
	return render(request, 'home.html', response)

def status(request): #untuk bikin obj model
	stat = The_Status(request.POST or None)
	if (request.method == 'POST' and stat.is_valid()):
			
		response['status'] = request.POST['status']
				
		a_status = Status(my_status= response['status'])
		a_status.save()
				
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('')

def profile(request):
	return render(request, 'profile.html', response)

def books(request):
	return render(request, 'books.html', response)

def read(request):
	link = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
	return JsonResponse(link)

def validate(request):
	if request.method == 'POST':
		email = request.POST['email']
		check = Subscribe.objects.filter(email=email)
		if check.exist():
			return JsonResponse({'exist':True})
		else:
			return JsonResponse({'exist':False})
