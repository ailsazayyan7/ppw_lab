from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Status
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Lab6UnitTest(TestCase):

	def test_lab_6_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_if_there_is_Hello_apa_kabar(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn("Hello, apa kabar?", html_response)

	def test_lab6_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		#creating a new activity
		new_activity = Status.objects.create(my_status='the status')

		#retrieving all available activity
		counting_all_available_status=Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

class Lab7FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		self.selenium = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
		super(Lab7FunctionalTest, self).setUp()

	def tearDown(self):
		self.drover.quit()
		super(FunctionalTest, self).tearDown()

	"""def test_input_todo(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://127.0.0.1:8000')
		
		# find the form element
		status = selenium.find_element_by_name('status')
		
		# Fill the form with data
		status.send_keys('Coba Coba')
		
		# submitting the form
		status.submit()
		self.assertIn('Coba Coba', driver.page_source)
		time.sleep(5)

	def test_profile_title(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/profile/')
		title = self.selenium.find_element_by_class_name('header').text
		self.assertIn('My Profile', title)

	def test_if_there_is_nama_in_profile(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/profile/')
		nama = self.selenium.find_element_by_id('nama').text
		self.assertIn('Nama', nama)

	def test_body_font_is_Arial(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/profile/')
		font = self.selenium.find_element_by_tag_name('body').value_of_css_property('font-family')
		self.assertEqual('Arial', font)

	def test_header_color_is_white(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/profile/')
		border = self.selenium.find_element_by_class_name('header').value_of_css_property('border-style')
		self.assertEqual('solid', border)"""