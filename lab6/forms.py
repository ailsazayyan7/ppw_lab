from django import forms

class The_Status(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    status = forms.CharField(label = 'your status', required = True, max_length = 300, widget = forms.TextInput(attrs=attrs))

"""class Subscriber(froms.Form):
	attrs = {
	'class' : 'form-control'
	}
	email = forms.EmailField(label = 'email', required = True, widget = forms.EmailInput(attrs = attrs))
	name = forms.CharField(label = 'name', required = True, max_length=27, widget = forms.TextInput(attrs=attrs))
	password = forms.CharField(label = 'pass', required = True, max_length=16 , widget = forms.PasswordInput(attrs=attrs)
"""