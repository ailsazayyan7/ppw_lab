$(function() {
  $(".view").on( "click", function() {
    $(this).next().slideToggle(250);
    $fexpand = $(this).find(">:first-child");
    if ($(this).hasClass('opened')) {
        $(this).removeClass('opened');
    } else {
        $(this).addClass('opened');
    };
  });

    var switchContainer = $('.switch-container');

    switchContainer.on('click', function() {
    var body      = $('body'),
        onSwitch  = $('.switch'),
        container = $('.data');
      
      $(this).toggleClass('on-indicator');
      onSwitch.toggleClass('switched-on');
      body.toggleClass('night-mode');
      container.toggleClass('night-mode-text');
    });
});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3);
}

function showPage() {
    document.getElementById("loading").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}
