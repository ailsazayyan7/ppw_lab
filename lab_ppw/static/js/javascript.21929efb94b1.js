$(function() {
  $(".view").on( "click", function() {
    $(this).next().slideToggle(250);
    $fexpand = $(this).find(">:first-child");
    if ($(this).hasClass('opened')) {
        $(this).removeClass('opened');
    } else {
        $(this).addClass('opened');
    };
  });
});

 
$(document).ready(function(){
    var color = true;
    $("#theme").click(function(){
        if(color) {
            $(".bg-dark").css("background-color", "#343a40");
            $("body").css("background-color", "#00000");
            color = false;
        }
        else {
            $(".bg-dark").css("background-color", "#00000");
            $("body").css("background-color", "#ffffff");
            color = true;
        }
    });
});
